//
//  ViewController.swift
//  SeeFoodAR
//
//  Created by Marko Živko on 16/07/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: part1
    @IBOutlet var imageView: UIImageView!
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title = "Slikaj i prepoznaj "
        //navigationController?.navigationBar.prefersLargeTitles = true
        
        imagePicker.delegate = self
        //take image by opening camera and shooting a photo
        imagePicker.sourceType = .camera
        //imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        
        
    }
    
    //MARK: part1
    @IBAction func cameraButtonPressed(_ sender: UIBarButtonItem) {
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: part2
    //after image is picked we need to send it to our machine learing model
    //and try to process this image
    //WE NEED delegate method - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let userPickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            //Xcode does not know the value of the key in dict
            imageView.image = userPickedImage
            
            //MARK: part3 - consuming image and converting to CII (Core Image Image)
            //special type which is used when using CoreML and Vision Framework
            guard let ciimage = CIImage(image: userPickedImage) else {
                fatalError("Could not convert image")
            }
            
            //MARK: part5 - calling a detect image
            detect(image: ciimage)
            
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
        //also we need to go to plist and add some permissions
        //Privacy-CamerUsageDescription
        //Privacy-PhotoLibraryUsage
        
    }
    
    //MARK: part4
    //using inceptionV3 model
    func detect(image: CIImage){
        
        //this line of code can throw an error
        guard let model = try? VNCoreMLModel(for: Inceptionv3().model) else{
            fatalError("Loading coreML failed!")
        }
        
        let request = VNCoreMLRequest(model: model) { (request, error) in
            
            guard let results = request.results as? [VNClassificationObservation] else{
                fatalError("Model failed to load/process image!")
            }
            
            //print(results)
            
            //MARK: part6 - tapping into most accurate (1st) result
            if let firstResult = results.first{
                self.title = "Picture of: \(firstResult.identifier)"
             }
            
        }
        
        let handler = VNImageRequestHandler(ciImage: image)
        
        
        //try! handler.perform([request])
        //better to do this below
        do{
            try handler.perform([request])
        }catch{
            print(error)
        }
        
    }
}

